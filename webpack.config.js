var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
    
module.exports = {
  entry: './dev/js/index.js',
  output: {
    path: __dirname + '/public/',
    filename: 'js/bundle.js'
  },
  publicPath: 'http://localhost:8080',
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [{
      test: /\.pug$/,
      loader: 'pug-html-loader'
    },{
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract('css!postcss-loader?parser=postcss-scss')
    },{
      test: /\.(jpe?g|gif|png|svg)$/,
      loader: 'file-loader?emitFile=false&name=../[path][name].[ext]'
    },{
      test: /\.(json)$/,
      loader: 'json-loader'
    },{
      test: /\.jsx?$/,
      exclude: [/node_modules/],
      loader: "babel-loader",
      query: {
        presets: ['es2015', 'react', 'stage-0', 'stage-1']
      }
    }]
  },
  postcss: function() {
    return [
        require('postcss-strip-inline-comments'),
        require('postcss-smart-import')({
          addDependencyTo: webpack
        }),
        require('precss'),
        require('postcss-extend'),
        require('postcss-functions')({
          functions: {
            em: function (value, size) {
              if(size === undefined) size = 16;
              value = parseInt(value);
              size = parseInt(size);
              if(value % size) {
                return(value / size).toFixed(2) + 'em';
              } else {
                return(value / size).toFixed(0) + 'em';
              }
            },
          }
        }),
        require('postcss-url'),
        require("postcss-cssnext")({
          browsers: ['> 1%', 'last 5 versions', 'IE 7']
        }),
        require('postcss-short'),
        require('postcss-browser-reporter'),
        require('postcss-reporter'),
        require('postcss-short'),
        require("cssnano"),
        require("css-mqpacker")
    ];
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: './dev/pug/index.pug'
    }),
    new ExtractTextPlugin('css/style.css'),
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    inline: true
  }
};



