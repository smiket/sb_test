import React, {Component} from 'react';
import {connect} from 'react-redux';


class App extends Component{
  showText() {
    if (this.props.inp === 'hidden') {
      this.props.onShow();
    } else {
      this.props.onHide();
    }
  }
  render() {
    return (
      <div className={this.props.inp}>
        <h1>Hello, World</h1>
        <p>Lorem ipsum</p>
        <button onClick={this.showText.bind(this)}>change</button>
      </div>
    );
  }
};


export default connect(
  state => ({
    inp: state
  }),
  dispatch => ({
    onShow: () => {
      dispatch({type: 'visible',payload: 'show'});
    },
    onHide: () => {
      dispatch({type: 'visible',payload: 'hidden'});
    }
  })
)(App);

